import numpy as np
import pandas as pd
import re
import datetime
import sys

infile = r"tmp/filterlog.log"


def keep_phrases_data(infile, phrases):
    logs = []
    with open(infile, "r") as f:
        f = f.readlines()

    for line in f:
        for phrase in phrases:
            if phrase in line:
                logs.append(line)
                break
    return logs


def pfsense_suricata_preprocessing(txt):
    Description = []
    Source = []
    Suricata_id = []
    Classification = []
    Priority = []
    Protocol = []
    Destination_IP_address = []
    input_IP_address = []
    Date = []
    for line in range(0, len(txt)):
        s1 = txt[line].split(' ', 4)[0]
        dt = datetime.datetime.strptime(s1, "%Y-%m-%dT%H:%M:%S%z")

        scr = txt[line].split(' ', 4)[1]
        Source.append(scr)

        suri_id = txt[line].split(' ', 4)[2]
        suri_id = (suri_id.replace('_', '')).replace(':', '').replace(':', '').replace('[', '').replace(']', '')
        Suricata_id.append(suri_id)

        str_tmp = txt[line].split(' ', 4)[4]

        Desc = str_tmp.split(' [Classification: ')[0]
        Description.append(Desc)

        Classi = (str_tmp.split(' [Classification: ')[1]).split(']')[0]

        Pr = ((str_tmp.split(' [Classification: ')[1]).split(']')[1]).split(' [Priority: ')[1]

        Protocol_type = (((str_tmp.split(' [Classification: ')[1]).split(']')[2]).split('{')[1]).split('}')[0]

        IPS = (((str_tmp.split(' [Classification: ')[1]).split(']')[2]).split('{')[1]).split('}')[1]
        Input_IP = IPS.split('->')[0]
        Input_IP = Input_IP.replace(' ', '')

        Destination_IP = IPS.split('->')[1]
        Destination_IP = (Destination_IP.replace('\n', '')).replace(' ', '')

        input_IP_address.append(Input_IP)
        Destination_IP_address.append(Destination_IP)

        Protocol.append(Protocol_type)
        Classification.append(Classi)
        Priority.append(Pr)

        Date.append(dt)

    df_with_classific_priority_attrs = pd.DataFrame(
        {'Description': Description, 'input_IP_address': input_IP_address,
         'Destination_IP_address': Destination_IP_address,
         'Protocol': Protocol, 'Classification': Classification, 'Priority': Priority})

    return df_with_classific_priority_attrs


def clean_text(text):
    # remove backslash-apostrophe
    text = re.sub("\'", "", text)

    text = re.sub("^.*  m m ", "", text)

    # remove everything except alphabets
    text = re.sub("[^a-zA-Z]", " ", text)
    # remove whitespaces
    text = ' '.join(text.split())
    # convert text to lowercase
    text = text.lower()

    text = re.sub("^.*errcode sc err ", "", text)

    return text


# keep data with "Classification" & "Priority" Attributes
Classification_Priority_Logs = keep_phrases_data(infile, ["Priority"])
# Create Dataframe with "Classification" & "Priority" Attributes
df_with_classific_priority_attrs = pfsense_suricata_preprocessing(Classification_Priority_Logs)

df_1 = df_with_classific_priority_attrs[df_with_classific_priority_attrs['Description'].notna()]
df_2 = df_1[df_1['Classification'].notna()]
df_2.dropna(axis=0, how='any', inplace=True)
df_2.drop_duplicates(inplace=True)
# clean textual data
df_2['Classification_cleaned'] = df_2.Classification.apply(lambda x: clean_text(x))
df_2['Description_cleaned'] = df_2.Description.apply(lambda x: clean_text(x))
df_2['Protocol_cleaned'] = df_2.Protocol.apply(lambda x: clean_text(x))
unique_cat = (np.array(df_2['Classification_cleaned'].unique())).shape[0]
df_2 = df_2.drop(['Classification', 'Description', 'Protocol'], axis=1)
df_2 = df_2[~df_2['Destination_IP_address'].astype(str).str.startswith('s')]
df_2 = df_2[~df_2['input_IP_address'].astype(str).str.startswith('s')]
print(unique_cat)

file_name = 'tmp/Label_Events' + '.csv'
df_2['Priority'].to_csv(file_name, columns=['Priority'], encoding='utf-8', index=False)

label_values = (np.array(df_2['Priority']))
with open('tmp/Label_Events' + '.txt', "w") as f1:
    np.savetxt(f1, label_values, delimiter=",", fmt="%s")
    f1.close()
df_2 = df_2.drop(['Classification_cleaned'], axis=1)
with open('tmp/Data_Events' + '.txt', "w") as f:
    np.savetxt(f, np.array(df_2), delimiter=",", fmt="%s")
    f.close()

file_name = 'tmp/Data_Events' + '.csv'
df_2.to_csv(file_name, columns=['input_IP_address', 'Destination_IP_address',
                                'Description_cleaned', 'Protocol_cleaned', 'Priority'], encoding='utf-8', index=False)
